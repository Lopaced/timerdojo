# Timer Dojo #

The tool's goal is to provide a simple graphical timer.

This app does a countdown from the session time (in minutes) to zero, and if needed, add an extra time (in order to make a compilable code).

## Timer App Parameters ##

```
-e,--extra-time <arg>     The session extra time in minutes.
-h,--help                 This help.
-s,--session-time <arg>   The session time in minutes.
-x,--x-position <arg>     The time X position on screen.
-y,--x-position <arg>     The time Y position on screen.
```

## Run Timer App with maven ##

```
mvn exec:java -Dexec.args="-s 5 -e 1"
```
Or
```
mvn exec:java -Dexec.args="--session-time 5 --extra-time 1"
```

## How to quit the Time App ##

Click on le `X` button with `CTRL` + `ALT`