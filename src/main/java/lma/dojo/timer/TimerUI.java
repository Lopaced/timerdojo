package lma.dojo.timer;

import static java.awt.event.ActionEvent.CTRL_MASK;
import static java.awt.event.ActionEvent.SHIFT_MASK;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class TimerUI {

	private final JLabel timeLabel;
	private final JFrame frame = new JFrame();

	private final JButton btnStartExtraTime;
	private final JButton btnRestartSession;
	private final JButton btnExit;
	private final JPanel pnlOptions;
	private final JPanel mainPnl;

	private boolean timeOverState = true;

	public TimerUI() {

		btnStartExtraTime = new JButton("extra time");
		btnExit = new JButton("X");
		btnRestartSession = new JButton("new session");

		pnlOptions = new JPanel();
		pnlOptions.setOpaque(false);
		pnlOptions.setVisible(false);
		pnlOptions.add(btnRestartSession);
		pnlOptions.add(btnStartExtraTime);
		pnlOptions.add(btnExit);

		timeLabel = new JLabel(" start timer...");
		timeLabel.setHorizontalAlignment(JLabel.CENTER);
		timeLabel.setVerticalAlignment(JLabel.CENTER);
		timeLabel.setPreferredSize(new Dimension(100, 25));
		timeLabel.addMouseListener(new MouseListener() {

			@Override
			public void mouseReleased(MouseEvent e) {
			}

			@Override
			public void mousePressed(MouseEvent e) {
			}

			@Override
			public void mouseExited(MouseEvent e) {
			}

			@Override
			public void mouseEntered(MouseEvent e) {
			}

			@Override
			public void mouseClicked(MouseEvent e) {
				showOptions(true);
			}
		});

		mainPnl = new JPanel();
		mainPnl.setLayout(new BorderLayout());
		mainPnl.add(timeLabel, BorderLayout.NORTH);
		mainPnl.add(pnlOptions, BorderLayout.CENTER);

		frame.add(mainPnl);
		frame.setUndecorated(true);
		frame.setAlwaysOnTop(true);
	}

	public void init(final TimerDojo timerDojo) {

		btnRestartSession.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				btnStartExtraTime.setEnabled(true);
				showOptions(false);
				timerDojo.startSessionTimer();
			}
		});

		btnStartExtraTime.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				btnStartExtraTime.setEnabled(false);
				showOptions(false);
				timerDojo.startExtraTimer();
			}
		});

		btnExit.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if ((e.getModifiers() & CTRL_MASK) == CTRL_MASK && (e.getModifiers() & SHIFT_MASK) == SHIFT_MASK) {
					System.exit(0);
				} else {
					showOptions(false);
				}
			}
		});

	}

	private void showOptions(boolean isVisible) {

		if (pnlOptions.isVisible() == isVisible) {
			return;
		}

		btnExit.requestFocus();

		pnlOptions.setVisible(isVisible);
		frame.pack();
	}

	public void display(Point localisation) {
		frame.pack();
		frame.setLocation(localisation);
		frame.setVisible(true);
	}

	public void setTimeText(String timeString) {
		timeLabel.setText(timeString);
	}

	public void setStyleTimeCasual() {
		timeLabel.setForeground(Color.black);
		mainPnl.setOpaque(false);
	}

	public void setStyleTimeWarnning() {
		timeLabel.setForeground(Color.red);
		mainPnl.setOpaque(false);
	}

	public void setStyleTimeOver() {
		timeLabel.setForeground(Color.black);
		mainPnl.setOpaque(true);
		mainPnl.setBackground(Color.red);
		showOptions(true);
	}

	public void setStyleExtraTimeOver() {
		
		timeOverState = !timeOverState;
		
		Color color = timeOverState ? Color.red : Color.yellow;
		timeLabel.setForeground(Color.black);
		mainPnl.setOpaque(true);
		mainPnl.setBackground(color);
		btnStartExtraTime.setEnabled(false);
		showOptions(true);
	}
}
