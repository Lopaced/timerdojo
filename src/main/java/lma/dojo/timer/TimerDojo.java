package lma.dojo.timer;

public interface TimerDojo {

	void startSessionTimer();

	void startExtraTimer();

  void updateRemindTimeInMillis(long remindMillis);
}
