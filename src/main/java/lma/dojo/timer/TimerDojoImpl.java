package lma.dojo.timer;

import static java.lang.String.format;
import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static java.util.concurrent.TimeUnit.MINUTES;

import java.util.concurrent.TimeUnit;

public class TimerDojoImpl implements TimerDojo {

	private final TimerUI timerUi;
	private final long sessionTime;
	private final long extraTime;
	private RefreshTask refreshTask;
	private boolean isExtraTimeModeEnable = false;
	private long lastRemindMillis;

	public TimerDojoImpl(TimerUI timerUi, RefreshTask refreshTask, long sessionTimeInMillis, long extraTimeInMillis) {
		this.timerUi = timerUi;
		this.refreshTask = refreshTask;
		extraTime = extraTimeInMillis;
		sessionTime = sessionTimeInMillis;
	}

	@Override
	public void startSessionTimer() {
		isExtraTimeModeEnable = false;
		refreshTask.startRefreshTask(sessionTime, this);
	}

	@Override
	public void startExtraTimer() {
		isExtraTimeModeEnable = true;
		refreshTask.startRefreshTask(extraTime + lastRemindMillis, this);
	}

	@Override
	public void updateRemindTimeInMillis(long remindMillis) {

		boolean extraTimeOver = (remindMillis < -1 * extraTime) || (remindMillis < 0 && isExtraTimeModeEnable);

		if (extraTimeOver) {
			timerUi.setTimeText(" Next !!");
			timerUi.setStyleExtraTimeOver();

		} else {

			timerUi.setTimeText(getTimeString(remindMillis));

			if (remindMillis < 0) {
				timerUi.setStyleTimeOver();

			} else if (remindMillis <= TimeUnit.MINUTES.toMillis(1)) {
				timerUi.setStyleTimeWarnning();

			} else {
				timerUi.setStyleTimeCasual();
			}
		}
		
		lastRemindMillis = remindMillis;
		
	}

	private String getTimeString(long timeInMillis) {

		long absTimeInMillis = Math.abs(timeInMillis);

		long minutes = MILLISECONDS.toMinutes(absTimeInMillis);
		long secondes = MILLISECONDS.toSeconds(absTimeInMillis) - MINUTES.toSeconds(minutes);

		if (timeInMillis < 0 && secondes > 0) {
			return format(" - %d min %d s", minutes, secondes);
		} else {
			return format(" %d min %d s", minutes, secondes);
		}
	}

}
