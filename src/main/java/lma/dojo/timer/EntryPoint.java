package lma.dojo.timer;

import java.awt.Point;
import java.util.concurrent.TimeUnit;

import org.apache.commons.cli.BasicParser;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

public class EntryPoint {

	public static void main(String[] args) throws ParseException {

		Options options = new Options();
		options.addOption("h", "help", false, "This help.");
		options.addOption("s", "session-time", true, "The session time in minutes.");
		options.addOption("e", "extra-time", true, "The session extra time in minutes.");
		options.addOption("x", "x-position", true, "The time X position on screen.");
		options.addOption("y", "x-position", true, "The time Y position on screen.");

		CommandLine cmd = new BasicParser().parse(options, args);

		if (!cmd.hasOption("s")) {
			HelpFormatter formater = new HelpFormatter();
			formater.printHelp("Dojo Timer", options);
			return;
		}

		int sessionMinutes = Integer.parseInt(cmd.getOptionValue("s"));
		int extraMinutes = cmd.hasOption("e") ? Integer.parseInt(cmd.getOptionValue("e")) : 0;
		int xPosition = cmd.hasOption("x") ? Integer.parseInt(cmd.getOptionValue("x")) : 200;
		int yPosition = cmd.hasOption("y") ? Integer.parseInt(cmd.getOptionValue("y")) : 200;

		final Long remindMillis = TimeUnit.MINUTES.toMillis(sessionMinutes);
		final Long extraMillis = TimeUnit.MINUTES.toMillis(extraMinutes);

		final TimerUI timerUi = new TimerUI();
		final RefreshTask refreshTask = new RefreshTask();
		final TimerDojo timerDojo = new TimerDojoImpl(timerUi, refreshTask, remindMillis, extraMillis);
		timerUi.init(timerDojo);
		timerUi.display(new Point(xPosition, yPosition));

	}
}
