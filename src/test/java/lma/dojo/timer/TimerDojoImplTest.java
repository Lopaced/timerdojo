package lma.dojo.timer;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import java.util.concurrent.TimeUnit;

import lma.dojo.timer.RefreshTask;
import lma.dojo.timer.TimerDojoImpl;
import lma.dojo.timer.TimerUI;

import org.junit.Before;
import org.junit.Test;

public class TimerDojoImplTest {

	private long sessionTimeInMillis = TimeUnit.MINUTES.toMillis(2);
	private long extraTimeInMillis = TimeUnit.MINUTES.toMillis(1);
	private TimerUI timerUiMock;
	private RefreshTask refreshTaskMock;
	private TimerDojoImpl tested;

	@Before
	public void setUp() {
		timerUiMock = mock(TimerUI.class);
		refreshTaskMock = mock(RefreshTask.class);

		tested = new TimerDojoImpl(timerUiMock, refreshTaskMock, sessionTimeInMillis, extraTimeInMillis);
	}

	@Test
	public void testStartSessionTimer() {
		tested.startSessionTimer();
		verify(refreshTaskMock).startRefreshTask(sessionTimeInMillis, tested);
	}

	@Test
	public void testStartSessionTimer_AvecTempsRestant() {

		tested.updateRemindTimeInMillis(3l);

		tested.startSessionTimer();

		verify(refreshTaskMock).startRefreshTask(sessionTimeInMillis, tested);
	}

	@Test
	public void testStartExtraTimer() {
		tested.startExtraTimer();
		verify(refreshTaskMock).startRefreshTask(extraTimeInMillis, tested);
	}

	@Test
	public void testStartExtraTimer_AvecTempsEmpute() {

		tested.updateRemindTimeInMillis(-3l);

		tested.startExtraTimer();

		verify(refreshTaskMock).startRefreshTask(extraTimeInMillis - 3, tested);
	}

	@Test
	public void testUpdateRemindTimeInMillis_AvecTempsSupMinute() {

		tested.updateRemindTimeInMillis(TimeUnit.MINUTES.toMillis(1) + 1);

		verify(timerUiMock).setTimeText(" 1 min 0 s");
		verify(timerUiMock).setStyleTimeCasual();
	}

	@Test
	public void testUpdateRemindTimeInMillis_AvecTempsInfMinute() {

		tested.updateRemindTimeInMillis(TimeUnit.MINUTES.toMillis(1) - 1);

		verify(timerUiMock).setTimeText(" 0 min 59 s");
		verify(timerUiMock).setStyleTimeWarnning();
	}

	@Test
	public void testUpdateRemindTimeInMillis_AvecExtraTimeImplicite0Seconde() {

		tested.updateRemindTimeInMillis(-1);

		verify(timerUiMock).setTimeText(" 0 min 0 s");
		verify(timerUiMock).setStyleTimeOver();
	}

	@Test
	public void testUpdateRemindTimeInMillis_AvecExtraTimeImplicite1Seconde() {

		tested.updateRemindTimeInMillis(TimeUnit.SECONDS.toMillis(1) * -1);

		verify(timerUiMock).setTimeText(" - 0 min 1 s");
		verify(timerUiMock).setStyleTimeOver();
	}

	@Test
	public void testUpdateRemindTimeInMillis_AvecExtraTimeImpliciteDepasse() {

		tested.updateRemindTimeInMillis(extraTimeInMillis * -1 - 1);

		verify(timerUiMock).setTimeText(" Next !!");
		verify(timerUiMock).setStyleExtraTimeOver();
	}

	@Test
	public void testUpdateRemindTimeInMillis_AvecExtraTimeConsomme() {

		// Consommation de l'extra-time
		tested.startExtraTimer();
		
		// test
		tested.updateRemindTimeInMillis(-1);

		verify(timerUiMock).setTimeText(" Next !!");
		verify(timerUiMock).setStyleExtraTimeOver();
	}
}
