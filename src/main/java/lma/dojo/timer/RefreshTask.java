package lma.dojo.timer;

import java.util.Timer;
import java.util.TimerTask;

public class RefreshTask {

	private final Timer timer = new Timer();
	private TimerTask refreshTask;
	private long remindMillis;

	public void startRefreshTask(final long timeInMillis, final TimerDojo timerListener) {

		remindMillis = timeInMillis;

		if (refreshTask != null) {
			refreshTask.cancel();
			timer.purge();
		}

		refreshTask = new TimerTask() {

			@Override
			public void run() {
				timerListener.updateRemindTimeInMillis(remindMillis);
				remindMillis -= 1000;
			}
		};

		timer.scheduleAtFixedRate(refreshTask, 0, 1000);
	}
}